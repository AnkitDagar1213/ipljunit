import java.io.*;
import java.util.*;

public class Ipl {
    private static String matchesFile;
    private static String deliveriesFile;

    public Ipl(String matchesFile, String deliveriesFile) {
        this.matchesFile = matchesFile;
        this.deliveriesFile = deliveriesFile;
    }
    public static Map<String,Integer> calculateMatchesPerYear(){

        Map<String, Integer> matchesPerYear = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String year = data[1];

                matchesPerYear.put(year, matchesPerYear.getOrDefault(year, 0) + 1);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return matchesPerYear;
        }
    }
    public static Map<String,Double> calculateTopEconomicalBowler (){
        Map<String, Double> economicalBowlers = new HashMap<>();
        ArrayList<String> teams_id = new ArrayList<>();
        Map<String, Integer> bowlerRuns = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();
        Map<String,Double> topEconomicalBowler = new HashMap<>();

        String bowlerName = "";
        double economy = Double.MAX_VALUE;

        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String season = data[1];
                if (season.equals("2015")) {
                    teams_id.add(data[0]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(deliveriesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {

                String[] data = row.split(",");
                String match_Id = data[0];
                if (teams_id.contains(match_Id) && data[9].equals("0")) {
                    int balls;
                    String bowler = data[8];
                    int runs = Integer.parseInt(data[17]);
                    int extras = Integer.parseInt(data[16]);
                    int wideRuns = Integer.parseInt(data[10]);
                    int byeRuns = Integer.parseInt(data[11]);
                    int legByeRuns = Integer.parseInt(data[12]);

                    if (wideRuns == 0 && byeRuns == 0 && legByeRuns == 0) {
                        balls = 1;
                    } else {
                        balls = 0;
                    }
                    if (bowlerRuns.containsKey(bowler)) {
                        bowlerRuns.put(bowler, bowlerRuns.get(bowler) + runs + extras);
                        bowlerBalls.put(bowler, bowlerBalls.get(bowler) + balls);
                    } else {
                        bowlerRuns.put(bowler, runs + extras);
                        bowlerBalls.put(bowler, balls);
                    }
                }
            }
            for (String bowler : bowlerRuns.keySet()) {
                int runs = bowlerRuns.get(bowler);
                int balls = bowlerBalls.get(bowler);
                double economyRate = (double) runs / (balls / 6.0);
                economicalBowlers.put(bowler, economyRate);
            }
            for (Map.Entry<String, Double> i : economicalBowlers.entrySet()) {
                if (i.getValue() < economy) {
                    bowlerName = i.getKey();
                    economy = i.getValue();
                }
            }
            topEconomicalBowler.put(bowlerName,economy);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return topEconomicalBowler;
        }
    }
    public static Map<String, Integer> calculateExtraRunPerTeam() {

        Map<String, Integer> extraRunPerTeam = new TreeMap<>();
        ArrayList<String> teams_id = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String season = data[1];
                if (season.equals("2016")) {
                    teams_id.add(data[0]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(deliveriesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {

                String[] data = row.split(",");
                String match_id = data[0];

                if (teams_id.contains(match_id)) {
                    String team = data[3];
                    int runs = Integer.parseInt(data[16]);

                    extraRunPerTeam.put(team, extraRunPerTeam.getOrDefault(team, runs) + runs);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return extraRunPerTeam;
        }
    }
    public static Map<String, Map<String, Integer>> calculateMatchesWonByTeamPerYear(){
        Map<String, Map<String, Integer>> matchesWonByTeamPerYear = new TreeMap<>();
        Map<String, Integer> teamWinsByYear = new TreeMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {
            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String year = data[1];
                String winner = data[9];
                if (winner == "") {
                    continue;
                }
                if (!matchesWonByTeamPerYear.containsKey(year)) {
                    matchesWonByTeamPerYear.put(year, new HashMap<>());
                }
                teamWinsByYear = matchesWonByTeamPerYear.get(year);
                teamWinsByYear.put(winner, teamWinsByYear.getOrDefault(winner, 0) + 1);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            {
                return matchesWonByTeamPerYear;
            }
        }
    }
    public static Map<String, Integer> calculateTossWinnerMatchWinner(){
        Map<String, Integer> tossWinnerMatchWinner = new TreeMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String tossWinner = data[6];
                String winner = data[9];
                if(tossWinner.equals(winner))
                {
                    tossWinnerMatchWinner.put(winner, tossWinnerMatchWinner.getOrDefault(winner, 0) + 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            return tossWinnerMatchWinner;
        }
    }
}
