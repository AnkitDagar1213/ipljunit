import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

public class Ipltest {
private Ipl iplDataObj;
private final String matchesFile = "src/main/java/mockData/mock_matches.csv";
private final String deliveriesFile = "src/main/java/mockData/mock_deliveries.csv";
private final Map<String, Integer> matchPlayed = new HashMap<>();
private final Map<String, Map<String, Integer>> winningMatchData = new TreeMap<>();
private final Map<String, Integer> extraRun = new TreeMap<>();
private final Map<String, Double> economicalBowlers = new TreeMap<>();
private final Map<String, Integer> tossWinnerMatchWinner = new TreeMap<>();

@Before
public void setUp() {
        iplDataObj = new Ipl(matchesFile, deliveriesFile);
        matchPlayed.put("2016", 1);
        matchPlayed.put("2017", 1);
        matchPlayed.put("2015", 1);
        matchPlayed.put("2013", 1);
        matchPlayed.put("2014", 1);

        Map<String, Integer> team2016 = new HashMap<>();
        team2016.put("Kolkata Knight Riders", 1);
        Map<String, Integer> team2017 = new HashMap<>();
        team2017.put("Sunrisers Hyderabad", 1);
        Map<String, Integer> team2015 = new HashMap<>();
        team2015.put("Rising Pune SuperGiants", 1);
        Map<String, Integer> team2013 = new HashMap<>();
        team2013.put("Kolkata Knight Riders", 1);
        Map<String, Integer> team2014 = new HashMap<>();
        team2014.put("Kolkata Knight Riders", 1);

        winningMatchData.put("2016", team2016);
        winningMatchData.put("2017", team2017);
        winningMatchData.put("2015", team2015);
        winningMatchData.put("2013", team2013);
        winningMatchData.put("2014", team2014);

        extraRun.put("Kolkata Knight Riders", 1);

        economicalBowlers.put("Z Khan", 4.0);

        tossWinnerMatchWinner.put("Kolkata Knight Riders", 3);
        }


    @Test
    public void testMatchesPerYear() {
        Map<String, Integer> result = iplDataObj.calculateMatchesPerYear();
        assertEquals(result, matchPlayed);
    }

    @Test
    public void testMatchesWonByTeamPerYear() {
        Map<String, Map<String, Integer>> result = iplDataObj.calculateMatchesWonByTeamPerYear();
        assertEquals(result, winningMatchData);
    }

    @Test
    public void testExtraRunPerTeam() {
        Map<String, Integer> result = iplDataObj.calculateExtraRunPerTeam();
        assertEquals(result, extraRun);
    }

    @Test
    public void testTopEconomicalBowler() {
        Map<String, Double> result = iplDataObj.calculateTopEconomicalBowler();
        assertEquals(result, economicalBowlers);
    }
    @Test
    public void testTossWinnerMatchWinner() {
        Map<String, Integer> result = iplDataObj.calculateTossWinnerMatchWinner();
        assertEquals(result, tossWinnerMatchWinner);
    }
}